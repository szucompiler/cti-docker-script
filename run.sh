docker rm -f ctiserver
#docker run -h ctiserver --name ctiserver -it ctitest:1a bash #
#
CURDIR=$(dirname $(dirname $(realpath $0)));
docker run -d -h ctiserver \
       --name ctiserver \
       -v ${CURDIR}/compiler-install:/home/app/compiler \
       -v ${CURDIR}/opencti-src/Scripts:/home/app/opencti-src/Scripts \
       -p 8085:80 \
       -it \
       ctitest:1a \
       bash /home/app/opencti-src/istoire/pre.sh
