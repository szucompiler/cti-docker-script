#!/usr/bin/env bash

check_ret() {
    if [ $ret -ne 0 ] ; then
	echo 'ret value failed';
	exit 1;
    fi
}


PERL_MM_USE_DEFAULT=1 perl -MCPAN -e 'CPAN::HandleConfig->edit("urllist", "unshift", "https://mirrors.tuna.tsinghua.edu.cn/CPAN/"); mkmyconfig'
echo cpan install -y XML::Simple CGI::Pretty;
cpan install -y XML::Simple CGI::Pretty;
ret=$?;
check_ret;

echo sudo cpan install -y XML::Simple XML::Parser CGI::Pretty;
sudo cpan install -y XML::Simple XML::Parser CGI::Pretty;
ret=$?;

adduser app;
mkdir -p /home/app;
chown -R app /home/app;
mkdir -p /var/www;

sudo -i -u www-data 'ssh-keygen -b 2048 -t rsa -q -N "" ';
mkdir -p /var/www/.ssh
chown -R www-data /var/www/.ssh 
chown -R www-data /var/www
usermod -aG www-data app;
usermod -aG app www-data;


echo sudo -u app -i "PERL_MM_USE_DEFAULT=1 perl -MCPAN -e 'CPAN::HandleConfig->edit(\"urllist\", \"unshift\", \"https://mirrors.tuna.tsinghua.edu.cn/CPAN/\"); mkmyconfig'";


echo "PERL_MM_USE_DEFAULT=1 perl -MCPAN -e 'CPAN::HandleConfig->edit(\"urllist\", \"unshift\", \"https://mirrors.tuna.tsinghua.edu.cn/CPAN/\"); mkmyconfig'" > /home/app/second.sh;
chmod +x /home/app/second.sh;
chmod a+r /home/app/second.sh;
sudo -u app -i /home/app/second.sh;
echo "Return = $ret";

echo sudo -u app -i cpan install XML::Simple CGI::Pretty XML::Parser;
sudo -u app -i cpan install XML::Simple CGI::Pretty XML::Parser;

echo 'init script finished.';
