FROM ubuntu:18.04
MAINTAINER jasonlu "lu.gt@163.com"

RUN echo 1
RUN sed -i s@/archive.ubuntu.com/@/mirrors.cloud.aliyuncs.com/@g /etc/apt/sources.list
RUN sed -i s/archive.ubuntu.com/mirrors.cloud.aliyuncs.com/g /etc/apt/sources.list && sed -i s/security.ubuntu.com/mirrors.cloud.aliyuncs.com/g /etc/apt/sources.list && apt-get update

# Add dependencies group A
RUN apt-get install -y perl gcc g++ make cmake libbison-dev flex

# Add dependencies group B
RUN apt-get install -y openjdk-8-jdk sudo git ssh


ENV TZ=Asia/Shanghai
#设置时区
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo '$TZ' > /etc/timezone

# Add dependencies group C
RUN apt-get install -y ksh perl php apache2 libperl4-corelibs-perl libapache2-mod-php libapache2-mod-perl2 php-xml libexpat1-dev openssh-server python python3 vim emacs

RUN apt-get install -y cpio

ADD ./docker-scripts/init.sh /home/app/init.sh

RUN /home/app/init.sh

RUN apt-get install -y gcc-7-arm-linux-gnueabi

# Add local files
ADD ssh-folder /home/app/.ssh
ADD opencti-src /home/app/opencti-src
ADD opencti-testware /home/app/opencti-testware
ADD deploy-conf /home/app/deploy-conf

# Change pre-execute
RUN rm -rf /root/.ssh && cp -r /home/app/.ssh /root/.ssh

